//
//  TimerModel.swift
//  TimerTestJob
//
//  Created by Gurwinder Singh on 5/18/18.
//  Copyright © 2018 Gurwinder Singh. All rights reserved.
//

import UIKit

class TimerModel: NSObject {
    
    var timerSeconds = Int()
    var timerIsTimerRunning = Bool()
    var timerResumeTapped = Bool()
    var timerTerminatingTime = String()

    override init() {
        
    }
    init(seconds:Int, IsTimerRunning:Bool, resumeTapped:Bool) {
        self.timerSeconds           = seconds
        self.timerIsTimerRunning    = IsTimerRunning
        self.timerResumeTapped      = resumeTapped
        self.timerTerminatingTime   = AppManager.m_GetHoursMinutesAndSecondsFrom()
    }
}
