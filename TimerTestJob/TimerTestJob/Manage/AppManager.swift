//
//  AppManager.swift
//  TimerTestJob
//
//  Created by Gurwinder Singh on 5/19/18.
//  Copyright © 2018 Gurwinder Singh. All rights reserved.
//

import UIKit

class AppManager: NSObject {
    class func m_GetHoursMinutesAndSecondsFrom() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:s"
        let calendar = Calendar.current
        let comp = calendar.dateComponents([.hour, .minute,.second], from: Date())
        let hour = comp.hour ?? 00
        let minute = comp.minute ?? 00
        let seconds = comp.second ?? 00
        return "\(hour):\(minute):\(seconds)"
    }
}
