//
//  Constant.swift
//  TimerTestJob
//
//  Created by Gurwinder Singh on 5/19/18.
//  Copyright © 2018 Gurwinder Singh. All rights reserved.
//

import Foundation

let gSaveModel  = "saveModel"
let gFirstLogin  = "firstLogin"
let gSeconds = "seconds"
let gIsTimerRunning = "is_timer_running"
let gIsResumeTapped = "is_resume_tapped"
let gTerminatingTime = "terminating_time"
let gPause = "Pause"
let gResume = "Resume"





