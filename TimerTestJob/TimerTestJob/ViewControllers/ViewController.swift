//
//  ViewController.swift
//  TimerTestJob
//
//  Created by Gurwinder Singh on 5/18/18.
//  Copyright © 2018 Gurwinder Singh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    //MARK: - Outlets -
    @IBOutlet weak var vwTimeBackGround: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnPause: UIButton!
    @IBOutlet weak var btnReset: UIButton!

    //MARK: - Objects -
    var seconds = 0
    var timer = Timer()
    var isTimerRunning = false
    var resumeTapped = false
    var parameters = NSMutableDictionary()
    var pausedDate = Date()
    
    
    //MARK: - Lifecycles -
    override func viewDidLoad() {
        super.viewDidLoad()
        addNotifications()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        m_CustomizeView()
        m_ResumeTimer()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    //MARK: - Notifications -
    func addNotifications() {
        // Notifications added to handle different states of application
        NotificationCenter.default.setObserver(self, selector: #selector(ViewController.m_SaveTimer), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.setObserver(self, selector: #selector(ViewController.m_SaveTimer), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.setObserver(self, selector: #selector(ViewController.m_ResumeTimer), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
//        NotificationCenter.default.setObserver(self, selector: #selector(ViewController.m_ResumeTimer), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.setObserver(self, selector: #selector(ViewController.m_SaveTimer), name: NSNotification.Name.UIApplicationWillTerminate, object: nil)
    }
    //MARK: - Custom -
    func m_CustomizeView() {
        // UI customization
        vwTimeBackGround.fullyRound(diameter: vwTimeBackGround.frame.size.width, borderColor: .white, borderWidth: 5.0)
        btnStart.makeCircular()
        btnPause.makeCircular()
        btnReset.makeCircular()
    }
    @objc func m_SaveTimer(){
        // Save Timer and other variables on termination of app
        self.parameters.removeAllObjects()
        self.parameters.setValue(seconds, forKey: gSeconds)
        self.parameters.setValue(isTimerRunning, forKey: gIsTimerRunning)
        self.parameters.setValue(resumeTapped, forKey: gIsResumeTapped)
        if resumeTapped {
            self.parameters.setValue(pausedDate, forKey: gTerminatingTime)
        }else{
            self.parameters.setValue(Date(), forKey: gTerminatingTime)
        }
        UserDefaults.GSDefault(setObject: self.parameters.mutableCopy(), forKey: gSaveModel)
    }
    @objc func m_ResumeTimer(){
        // retrive time details on resuming the application
        if UserDefaults.GSKeyExist(Key: gSaveModel) {
            let getTimer = UserDefaults.GSDefault(objectForKey: gSaveModel) as! NSDictionary
            if getTimer.value(forKey: gIsTimerRunning) as! Bool == true && getTimer.value(forKey: gIsResumeTapped) as! Bool == false {
                // if app suspended with running timer
                self.seconds = 0
                btnStart.isEnabled = false
                btnPause.isEnabled = true
                btnReset.isEnabled = true
                self.btnPause.setTitle(gPause,for: .normal)
                self.isTimerRunning = getTimer.value(forKey: gIsTimerRunning) as! Bool
                self.resumeTapped = getTimer.value(forKey: gIsResumeTapped) as! Bool
                
                let startDate = getTimer.value(forKey: gTerminatingTime) as! Date
                let endDate = Date()
                let calendar = NSCalendar.current
                let dateComponents = calendar.dateComponents([.second], from: startDate, to: endDate)
                let second = dateComponents.second
                self.seconds = getTimer.value(forKey: gSeconds) as! Int + second!
                if !timer.isValid{
                    runTimer()
                }
            }else if getTimer.value(forKey: gIsTimerRunning) as! Bool == false && getTimer.value(forKey: gIsResumeTapped) as! Bool == true {
                // if app suspended after pausing the timer
                self.seconds = 0
                btnStart.isEnabled = false
                btnPause.isEnabled = true
                btnReset.isEnabled = true
                self.btnPause.setTitle(gResume,for: .normal)
                self.isTimerRunning = false
                self.resumeTapped = true
                self.seconds = getTimer.value(forKey: gSeconds) as! Int
                lblTime.text = timeString(time: TimeInterval(self.seconds))
            }else{
                // If Timer is in initial stage
                btnStart.isEnabled = true
                btnPause.isEnabled = false
                btnReset.isEnabled = true
                self.btnPause.setTitle(gPause,for: .normal)
                self.isTimerRunning = false
                self.resumeTapped = false
                self.seconds = 0
                lblTime.text = timeString(time: TimeInterval(seconds))
            }
            print(getTimer)
        }else{
            // For first launch of app
            btnStart.isEnabled = true
            btnPause.isEnabled = false
            btnReset.isEnabled = true
            self.btnPause.setTitle(gPause,for: .normal)
            self.isTimerRunning = false
            self.resumeTapped = false
            self.seconds = 0
            lblTime.text = timeString(time: TimeInterval(seconds))
        }
    }
    //MARK: - Timer -
    func runTimer() {
        // start timer
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(ViewController.updateTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
        btnPause.isEnabled = true
    }
    @objc func updateTimer() {
        //Update UI with seconds
        seconds += 1
        lblTime.text = timeString(time: TimeInterval(seconds))
    }
    
    func timeString(time:TimeInterval) -> String {
        // converting seconds into hours, minutes and seconds
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }

    //MARK: - Actions -
    @IBAction func actionStart(_ sender: Any) {
        // start timer
        if isTimerRunning == false {
            runTimer()
            self.btnStart.isEnabled = false
        }
    }
    @IBAction func actionPause(_ sender: Any) {
        //pause timer
        if self.resumeTapped == false {
            timer.invalidate()
            pausedDate = Date()
            isTimerRunning = false
            self.resumeTapped = true
            self.btnPause.setTitle(gResume,for: .normal)
        } else {
            runTimer()
            self.resumeTapped = false
            isTimerRunning = true
            self.btnPause.setTitle(gPause,for: .normal)
        }
    }
    @IBAction func actionReset(_ sender: Any) {
        // clear timer
        timer.invalidate()
        seconds = 0
        lblTime.text = timeString(time: TimeInterval(seconds))
        isTimerRunning = false
        resumeTapped = false
        btnPause.isEnabled = false
        btnStart.isEnabled = true
        self.btnPause.setTitle(gPause,for: .normal)
    }
    
}

